# wpdoctor

===

Hands on test for Dan Harrison.

Installation
---------------
1.  clone this git repo
2.  when cloned instead of wpdoctor-master remove -master from the folder name so it should be wpdoctor
3.  inside the cloned repo is an sql file called wpdoctor.sql create a database name it wpdoctor and import it there
4.  copy the clone repo and put it in htdocs , www or root of the local environment
5.  run local server and access the test site  localhost/wpdoctor/ . The username : edyljobs and password is : codemore
6.  you can use the shortcode anywhere now [dan_pricing_table] 

### Requirements 
xampp , wampp or other local environment

### Description
I used scss + BEM you can locate the non minified scss in wpdoctor/wp-content/plugins/wpdrs-coding-test-plugin/_src/scss/frontend/_pricing_table.scss

I also already created the ACF test and made the content fields on frontend dynamic from ACF repeater field, also the css file is minified already.

Thanks ,
Hoping to be part of the team,
Edyl