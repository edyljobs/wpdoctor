<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wpdoctor' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Xa0H;gVH$UY{U(w_sN6AT&WEKH;dLM%`bYOd-v_h_J oupp$EpI%x=!IR:#mrQa+' );
define( 'SECURE_AUTH_KEY',  '*?{Z/-.Nz6DkWuzxB9xFkRv^3< pK]3THxb6y$B+*@t$i&lup^ n[AI;P$boRkPe' );
define( 'LOGGED_IN_KEY',    '7SU+tjuq1A~QX^9*vTJzU&LV)VUDIe,g?|;G&=Sx0l4{c<%Y>p?C37Zr,^X%MK%q' );
define( 'NONCE_KEY',        'R>ls .p8_y`C}@yvnPU$S.6Y!y}./Z?GUu@8moY>pgjP2eH-k>`9-`)dm})&iw_a' );
define( 'AUTH_SALT',        'luiCB|<:?/454hA@ZV}@4_+VKzkd5*XO@n5~EKq0Q^9U]p<~#WP70x,==,YpfP o' );
define( 'SECURE_AUTH_SALT', 'Wu407cMO5V!H/@9FiAo8<C4`wPhWgW-?)u8 ^rW?&ZY}Mt7_-t4J$n%cG*0_^1 W' );
define( 'LOGGED_IN_SALT',   'M|x`_-dIyRGgZCj]-ePV!4`zq+IiXS~XkUvs?5OfI!~;_N9g[8QQU?5,w!ij!>u2' );
define( 'NONCE_SALT',       'LV!LDv_b#{:Uh$pY`O(.U0V[kQ(8 #,-C}9x$#4xg?5/vI:TS`/8g)I(Gx=0CJ[0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
