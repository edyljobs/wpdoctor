<?php 

/**
 * Class that sets up the core functionality of the plugin. 
 */
class WPDCT_Core extends WPDRS_Core
{
    /**
     * Store a copy of the version.
     * @var string
     */
    protected $version;
    
    
    /**
     * Function called when this class is initialised. This is where the plugin
     * sets up hooks, etc.
     *
     * @param $pluginBase string Contains the core __FILE__ reference.
     * @param string $version The current version number of the plugin.
     */
    public function __construct($pluginBase, $version)
    {
        parent::__construct($pluginBase);
        $this->version = $version;
        
        // Set up WordPress core init.
        add_action('init',  [&$this, '__init']);
    }
    
    
    /**
     * Method called on WordPress init.
     */
    public function __init() 
    {
        
    }
    
    
    /**
     * Return the version string.
     * @return string
     */
    public function getVersion() {
        return $this->version;
    }
    
    
    /**
     * Public factory method to initialise this plugin.
     * 
     * @param string $pluginBase The value of __FILE__
     * @param string $version The current version number of the plugin.
     * @return WPDCT_Core The initialised object.
     */
    public static function initialise($pluginBase, $version)
    {
        return new WPDCT_Core($pluginBase, $version);
    }
    
}