<?php 

/**
 * Class that sets up the functionality of the plugin that works on the frontend of the website. 
 */
class WPDCT_Frontend
{
    /**
     * Store a reference to the core object.
     * @var WPDCT_Core
     */
    protected $pluginCore;
    
    
    /**
     * Function called when this class is initialised. This is where the plugin
     * sets up hooks, etc.
     * 
     * @param WPDCT_Core $pluginCore
     */
    public function __construct($pluginCore)
    {
        $this->pluginCore = $pluginCore;
        
        // Set up WordPress core init.
        add_action('init',  [&$this, '__init']);

    }
    
    
    /**
     * Method called on WordPress init.
     */
    public function __init()
    {
        // Scripts and Styles
        add_action('wp_enqueue_scripts',   array(&$this, '__init_scriptsAndStyles'));
        
        // Shortcodes
        add_shortcode( 'dan_pricing_table', array(&$this, 'pricingTable'));
    }
    
    
    /**
     * Enqueue frontend scripts and styles.
     */
    public function __init_scriptsAndStyles()
    {
        $plugins_url = $this->pluginCore->__plugin_getPluginPath();
        
        // Core CSS
        // wp_enqueue_style('wpdct-frontend', $plugins_url . '/_src/scss/frontend.css', [], $this->pluginCore->getVersion() );
        wp_enqueue_style('wpdct-frontend', $plugins_url . '/assets/css/frontend.min.css', [], $this->pluginCore->getVersion() );
        
        // Core JS
        //wp_enqueue_script('wpdct-frontend', $plugins_url . '/asset/js/frontend.min.js', ['jquery'], $this->pluginCore->getVersion() );
    }
    
    // Edyl Added
    public function pricingTable()
    {

        $rows = get_field('table_plan');
        ?>
        
        <?php if( have_rows('table_plan', 'option') ): ?>

            <div class="mainpricing-container">

            <?php while( have_rows('table_plan', 'option') ): the_row(); ?>

             <div class="mainpricing-container__plans">
                    <div class="mainpricing-container__plans__heading">
                            <?php if(get_sub_field('plan_title') == "Pro Plan"): ?>
                                      <p class="popular">Popular</p>
                            <?php endif; ?>
                            <h1><?php the_sub_field('plan_title'); ?></h1>
                            <p><?php the_sub_field('plan_sub_text'); ?></p>
                    </div>
                    <div class="mainpricing-container__plans__contentlist">
                            <ul>
                                <?php while( have_rows('plan_features', 'option') ): the_row(); ?>
                                    <li><?php the_sub_field('plan_feature_title'); ?></li>
                                <?php endwhile; ?>
                            </ul>
                    </div>    
                    <div class="mainpricing-container__plans__price">
                            <span class="dollar">$</span>
                            <span class="number"><?php the_sub_field('cost'); ?></span>
                            <span class="month"><?php the_sub_field('subscription_type'); ?></span>
                    </div>  

                    <div class="mainpricing-container__plans__button">
                        <a href="<?php the_sub_field('button_link'); ?>" class="btn">Select</a>
                        <p>
                            <?php the_sub_field('guarantee_text'); ?>
                        </p>
                    </div>                                     
             </div>
    
            <?php endwhile; ?>
        
        </div>
        
        <?php endif; ?>
        
       <?php
    }
    // End edyl added
    
    
    /**
     * Public factory method to initialise this plugin.
     * 
     * @param WPDCT_Core $pluginCore
     */
    public static function initialise($pluginCore)
    {
        return new WPDCT_Frontend($pluginCore);
    }
    
}