<?php
if (!defined('ABSPATH')) 
    exit;

    
//
// Global Autoloader to pre-load classes on demand.
//
spl_autoload_register(function($class)
{
    static $classes = null;
    if ($classes === null)
    {
        $rootPath = dirname(__FILE__) . '/';
        $classes = array(
            
            // WP Doctors - Base Classes
            'WPDRS_Core'                                => $rootPath . 'lib/wpdrs_core.inc.php',            
            
            // Plugin Core Classes
            'WPDCT_Core'                                => $rootPath . '/WPDCT_Core.php',
            'WPDCT_Frontend'                            => $rootPath . '/WPDCT_Frontend.php',
        );
    }
    
    if (isset($classes[$class])) {
        require $classes[$class];
    }
});