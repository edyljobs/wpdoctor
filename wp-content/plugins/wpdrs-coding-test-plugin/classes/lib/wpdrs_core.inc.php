<?php 

/**
 * Class that provides functions for managing the plugin, settings and database.
 */
if (!class_exists('WPDRS_Core')) { class WPDRS_Core
{   
    /**
     * Stores a cache of the settings (settingskey => settings). 
     * @var array
     */
    protected $settingsStore;
    
    /**
     * Stores the plugin path.
     * @var string.
     */
    public $store_pluginPath;
    
    /**
     * Stores the base path for the plugin (the core __FILE__ string).
     * @var string
     */
    public $store_pluginBase;
    
    
    /**
     * Base constructor
     * 
     * @param $pluginBase string Contains the core __FILE__ reference.
     */
    protected function __construct($pluginBase)
    {
        // Set up the settings store, ready for use.
        $this->settingsStore = array();
        
        // Set up base URL for plugin and store the raw __FILE__ path
        $this->store_pluginBase = $pluginBase;
        $this->store_pluginPath = plugins_url('', plugin_basename($pluginBase));	
    }
    
    
    
    
    
    /**
     * Method that updates the database with our settings, using json_encode to correctly add it.
     * 
     * @param string $settingsKey The key used for saving the settings.
     * @param array $dataToSave The list of data to save.
     * 
     * @throws Exception If we don't have a valid settings key.
     */
    public function settings_saveSettings($settingsKey, $dataToSave)
    {
        // Dev Protection - ensure we've got a valid key
        if (empty($settingsKey)) {
            throw new Exception('Dev Error: Settings key cannot be empty, otherwise data cannot be saved or retrieved');
        }
        
        // ### Step 1 - Ensure we have a valid array to save
        if (empty($dataToSave)) {
            $dataToSave = array();
        }
        
        // ### Step 2 - Update the database with the new data 
        // We don't return this value for success or failure, because the return value is confusing.
        // A false is returned even if nothing is changed, which is odd. So we'd end up showing an error
        // when nothing has gone wrong at all. This is from WP docs:
        //
        // update_option() True if option value has changed, false if not or if update failed.
        //
        $result = update_option($settingsKey, json_encode($dataToSave));
        
        // ### Step 3 - Update the local cache with what we have
        $settingsStore[$settingsKey] = $dataToSave;
    }
    
    
    /**
     * Method that updates the database with with a single setting (not all settings), 
     * preserving the other settings.
     *
     * @param string $settingsKey The key used for saving the settings.
     * @param string $keyName The key within the settings to use for this single setting.
     * @param mixed $data The data to save to the database for this key.
     *
     * @throws Exception If we don't have a valid settings key.
     */
    public function settings_saveSetting($settingsKey, $keyName, $data)
    {
        // Dev Protection - ensure we've got a valid key
        if (empty($settingsKey)) {
            throw new Exception('Dev Error: Settings key cannot be empty, otherwise data cannot be saved or retrieved');
        }
        
        // ### Step 1 - Get latest version of settings
        $dataToSave = $this->settings_getAllSettings($settingsKey, true);
        
        // ### Step 2 - Merge our changes with the existing settings.
        $dataToSave[$keyName] = $data;
        
        // ### Step 3 - Update all settings with the new changes
        $this->settings_saveSettings($settingsKey, $dataToSave);
    }
    
    
    /**
     * Get a single setting from the database (using the settings cache).
     * 
     * @param string $settingsKey The key for the settings stored in the database.
     * @param string $singleSettingKey The key to use for this setting.
     * @param boolean $defaultValue The default value if no setting was found.
     * @param boolean $force If true, override the cache with a fresh load from the database.
     * 
     * @return string The value of the setting.
     */
    public function settings_getSetting($settingsKey, $singleSettingKey, $defaultValue = false, $force = false)
    {
        // Get all settings before trying to return this specific value.
        $allSettings = $this->settings_getAllSettings($settingsKey, $force);
        
        if (isset($allSettings[$singleSettingKey])) {
            return $allSettings[$singleSettingKey];
        }
        
        return $defaultValue;
    }
    
    
    
    /**
     * Get all settings form the database for the specified settings key. This 
     * method will cache the settings to reduce database accesses later.
     * 
     * @param string $settingsKey The key to use for fetching the settings.
     * @param boolean $force If true, override the cache with a fresh load from the database.
     * 
     * @return array Return a list of the settings as an associative array.
     * @throws Exception If we don't have a valid settings key.
     */
    public function settings_getAllSettings($settingsKey, $force = false)
    {
        // Dev Protection - ensure we've got a valid key
        if (empty($settingsKey)) {
            throw new Exception('Dev Error: Settings key cannot be empty, otherwise data cannot be saved or retrieved');
        }
        
        
        // ### Step 1 - Return from the cache if we've got the cache.
        if (!$force && isset($settingsStore[$settingsKey])) {
            return $settingsStore[$settingsKey];
        }
        
        // ### Step 2 - If we're here, we need to load the details from the database. 
        //              We'll decode the details from json if we have anything. 
        $rawSettings = get_option($settingsKey);
        if (!empty($rawSettings)) {
            $rawSettings = json_decode($rawSettings, true);
        } 
        
        // ### Step 3 - No settings found, so return a valid, but empty array.
        else {
            $rawSettings = array();
        }
        
        // ### Step 4 - Update the cache with what we have
        $settingsStore[$settingsKey] = $rawSettings;
        
        // ### Step 5 - Finally return what we have
        return $rawSettings;
    }
    
    
    
    /**
     * Install a table for this plugin using WordPress' dbDelta() function.
     * 
     * @param String $SQL The core SQL to create or upgrade the table
     * @return boolean True if the upgrade worked, false otherwise.
     */
    public static function __database_upgradeTable($SQL)
    {
        global $wpdb;
        $wpdb->show_errors();
                
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($SQL);
        
        return (empty($wbdb->last_error));
    }

    
    
    
 
    /**
     * Debug function - shows an array on the screen, and in the PHP error log for debug purposes.
     * @param array $data The array of data to show and display.
     * @param string $label If specified, show this as a label for the data that's been shown to make it clearer what the data applies to.
     * @param bool $errorlogOnly If true, do not echo the data to the screen, error log only.
     */
    public static function __debug_showArray($data, $label = false, $errorLogOnly = true)
    {
        // Break out a preformatted array
        $debugData = print_r($data, true);
        
        // Add a label prefix if there is one
        if ($label) {
            $debugData = $label . ":\n" . $debugData;
        }
        
        // Show to the screen if available.
        if (!$errorLogOnly)
        {
            printf('<div><pre style="padding: 20px; background: #efefef; border: 2px solid #ddd; margin: 20px;">%s</pre></div>', htmlentities($debugData));
        }
        
        // Show in the PHP error log
        error_log($debugData);
    }
    
    
    /**
     * Simple safe function to get an array value first checking it exists.
     *
     * @param Array array The array to retrieve a value for.
     * @param String $key The key in the array to check for.
     * @param String $default The default to return if the value wasn't found.
     *
     * @return String The array value for the specified key if it exists, or false otherwise.
     */
    public static function __arrays_getValue($array, $key, $default = false, $clean = true)
    {
        if (isset($array[$key])) 
        {
            $rawData = $array[$key];
            
            // If in cleaning mode, remove tags and slashes
            if ($clean) 
            {
                // Clean data in an array if needed.
                if (is_array($rawData)) 
                {
                    $rawData = array_map('strip_tags', $rawData);
                    return array_map('stripslashes', $rawData);
                } 
                else {
                    return stripslashes(strip_tags($rawData));
                }
            } 
            else {
                return $rawData;
            }
        }
        
        return $default;
    }
    
    
    /**
     * Simple safe function to get a number from an array, returning a default if the number returned
     * is not a valid number.
     *
     * @param Array array The array to retrieve a value for.
     * @param String $key The key in the array to check for.
     * @param integer $default The default integer to return if the value wasn't found or isn't a number.
     *
     * @return integer The array value for the specified key as an integer
     */
    public static function __arrays_getNumber($array, $key, $default = 0)
    {
        if (isset($array[$key]))
        {
            // Validate that it's a genuine integer
            if (preg_match('/^[0-9]+$/', $array[$key])) {
                return $array[$key] + 0;
            }
        }
        
        return $default;
    }
    
    
    /**
     * Get the URL for the plugin path including a trailing slash.
     * @return String The URL for the plugin path.
     */
    public function __plugin_getPluginPath() 
    {
        if (empty($this->store_pluginPath)) {
            throw new Exception ('Dev Error: Plugin Path needed');
        }
        
        return $this->store_pluginPath;
    }
    

}}