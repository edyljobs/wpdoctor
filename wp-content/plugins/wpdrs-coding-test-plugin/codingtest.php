<?php
/*
 Plugin Name:   WP Doctors Coding Test
 Plugin URI:    https://www.onlinejobs.ph/jobseekers/info/249293
 Description:   Edyl Jay Templado
 Version:       1.00
 Author:        Edyl Jay Templado (edyljobs@gmail.com)
 Author URI:    http://greatboyaquarium.com/portfolio/
 License:       Apache 2
 */

define('WPDCT_VERSION',   '1.17');

// Set up plugin autoloader.
include_once 'classes/_autoloader.php';

// Kick off core initialisation.
$wpdct_core = WPDCT_Core::initialise(__FILE__, WPDCT_VERSION);

// Kick off frontend initialisation
WPDCT_Frontend::initialise($wpdct_core);


